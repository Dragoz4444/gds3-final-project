﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwap : MonoBehaviour {
    [SerializeField] Transform one, two, cam;
    public bool boxMode;
    public bool transitioningCameraToBox;
    public bool transitioningCameraFromBox;
    public GameObject playerObject;


	
	// Update is called once per frame
	void Update () {
        if (transitioningCameraToBox == true && transitioningCameraFromBox == false)
            {
                cam.transform.position = one.position;
                cam.transform.rotation = one.rotation;
                boxMode = true;
                playerObject.SetActive(false);
                Cursor.lockState = CursorLockMode.None;     
        }
        else if (transitioningCameraFromBox == true && transitioningCameraToBox == false)
        {
            cam.transform.position = two.position;
            cam.transform.rotation = two.rotation;
            playerObject.SetActive(true);
            boxMode = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
}
