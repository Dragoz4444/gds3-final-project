﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScreenScript : MonoBehaviour {

    private float timeTakenToFinish;
    public Text timerText;


	// Use this for initialization
	void Start () {
        timeTakenToFinish = Time.realtimeSinceStartup;
        Debug.Log(timeTakenToFinish);

        string minutes = ((int)timeTakenToFinish / 60).ToString();
        string seconds = (timeTakenToFinish % 60).ToString("f2");

        timerText.text = minutes + ":" + seconds;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void QuitGame()
    {
        Application.Quit();
    }
}
