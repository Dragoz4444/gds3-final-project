﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemZoomView : MonoBehaviour {

    public float rotSpeed = 80;
    public GameObject bigCamera;
    private Vector3 itemorigin;
    private Quaternion itemoriginalRot;
    public GameObject ItemCamPos;

    //on mouse down, move item to ItemCamPos's location and enable zoomed in /rotation view, 
    //then when clicking the UI exit button move it back.

    void Start()
    {
        itemorigin = transform.position;
        itemoriginalRot = transform.rotation;
    }

    void OnMouseDown()
    {
        if (bigCamera.GetComponent<CameraSwap>().boxMode == false)
        {
            if (bigCamera.GetComponent<itemCameraSwap>().itemMode == false)
            {
                bigCamera.GetComponent<itemCameraSwap>().transitioningCameraToItem = true;
                transform.position = ItemCamPos.transform.position;
                transform.rotation = ItemCamPos.transform.rotation;
            }
        }
    }

    private void OnMouseDrag()
    {
        if (bigCamera.GetComponent<CameraSwap>().boxMode == false)
        {
            if (bigCamera.GetComponent<itemCameraSwap>().itemMode == true)
            {
                float rotX = Input.GetAxis("Mouse X") * rotSpeed * Mathf.Deg2Rad;
                float rotY = Input.GetAxis("Mouse Y") * rotSpeed * Mathf.Deg2Rad;
                transform.Rotate(Vector3.up, rotX, Space.World);
                transform.Rotate(Vector3.right, -rotY, Space.World);
            }
        }
    }

    private void Update()
    {
        if (bigCamera.GetComponent<itemCameraSwap>().transitioningCameraFromItem == true)
        {
            transform.position = itemorigin;
            transform.rotation = itemoriginalRot;
        }
    }
}
