﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audiomanager : MonoBehaviour {
    //background rain
    public AudioClip backgroundsound;
    public AudioSource backgroundsource;

    //puzzle box sounds
    public AudioClip wronganswersound;
    public AudioSource wronganswersource;

    public AudioClip bellsound;
    public AudioSource bellsource;

    public AudioClip buttonpressedsound;
    public AudioSource buttonpressedsource;

    //music puzzle sounds
    public AudioClip akeysound;
    public AudioSource akeysource;
    public AudioClip bkeysound;
    public AudioSource bkeysource;
    public AudioClip ckeysound;
    public AudioSource ckeysource;
    public AudioClip dkeysound;
    public AudioSource dkeysource;
    public AudioClip ekeysound;
    public AudioSource ekeysource;
    public AudioClip fkeysound;
    public AudioSource fkeysource;
    public AudioClip gkeysound;
    public AudioSource gkeysource;

    // Use this for initialization
    void Start ()
    {
        backgroundsource.clip = backgroundsound;
        wronganswersource.clip = wronganswersound;
        bellsource.clip = bellsound;
        buttonpressedsource.clip = buttonpressedsound;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (backgroundsource.isPlaying == false)
        {
            backgroundsource.Play();
        }
	}
}
