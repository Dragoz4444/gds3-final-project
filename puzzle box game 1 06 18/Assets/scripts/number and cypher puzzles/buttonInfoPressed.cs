﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonInfoPressed : MonoBehaviour {
    void OnMouseDown()
    {
        this.transform.parent.SendMessage("ButtonInfoEntered", this.name);
    }
}
