﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zoomview : MonoBehaviour
{
    float rotSpeed = 20;

    void OnMouseDown()
    {
        GameObject.Find("Main Camera").GetComponent<CameraSwap>().boxMode = true;
    }


    
        void OnMouseDrag()  {
            if (GameObject.Find("Main Camera").GetComponent<CameraSwap>().boxMode == true) {
            float rotX = Input.GetAxis("Mouse X") * rotSpeed * Mathf.Deg2Rad;
            float rotY = Input.GetAxis("Mouse Y") * rotSpeed * Mathf.Deg2Rad;

            transform.Rotate(Vector3.up, -rotX);
            transform.Rotate(Vector3.right, rotY);
        }
    }
}