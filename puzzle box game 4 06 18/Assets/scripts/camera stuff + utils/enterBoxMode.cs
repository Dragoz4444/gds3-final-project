﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enterBoxMode : MonoBehaviour {

    public GameObject bigCamera;

    void OnMouseDown()
    {
        if (bigCamera.GetComponent<itemCameraSwap>().itemMode == false)
        {
            bigCamera.GetComponent<CameraSwap>().transitioningCameraToBox = true;
        }
    }
}
