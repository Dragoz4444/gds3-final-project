﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class pausemenu : MonoBehaviour {

    public GameObject playercampos;
    public GameObject pauseMenuUI;
    public GameObject bigCamera;
    public GameObject clickdisabler;
    public AudioMixer audioMixer;

    public static bool GameIsPaused = false;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }

        if (GameIsPaused)
        {
            pauseMenuUI.SetActive(true);
            clickdisabler.SetActive(false);
            Time.timeScale = 0f;
            Cursor.lockState = CursorLockMode.None;
        }
        if (!GameIsPaused)
        {
            pauseMenuUI.SetActive(false);
            Time.timeScale = 1f;
            clickdisabler.SetActive(true);
            if (bigCamera.GetComponent<CameraSwap>().boxMode == false && bigCamera.GetComponent<itemCameraSwap>().itemMode == false)
            {
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
    }

    public void InvertYAxis()
    {
        if (playercampos.GetComponent<MouseLook>().invertY == false)
        {
            playercampos.GetComponent<MouseLook>().invertY = true;
            Debug.Log(playercampos.GetComponent<MouseLook>().invertY);
        }
        if (playercampos.GetComponent<MouseLook>().invertY == true)
        {
            playercampos.GetComponent<MouseLook>().invertY = false;
        }
    }

    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("I work! quitting game");
    }

    public void Resume()
    {
        GameIsPaused = false;
        Debug.Log("I work! resuming game");
    }
    public void Pause()
    {
        GameIsPaused = true;
        Debug.Log("I work! pausing game");
    }

    public void SetVolume(float volume)
    {
        audioMixer.SetFloat("volume", volume);
    }
}
