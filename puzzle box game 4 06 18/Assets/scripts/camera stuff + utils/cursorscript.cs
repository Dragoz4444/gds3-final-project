﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cursorscript : MonoBehaviour {

    public GameObject bigCamera;
    public GameObject crosshairs;

	// Update is called once per frame
	void Update () {
		if (bigCamera.GetComponent<itemCameraSwap>().itemMode == false && bigCamera.GetComponent<CameraSwap>().boxMode == false)
        {
            crosshairs.SetActive(true);
        }else
        {
            crosshairs.SetActive(false);
        }

    }
}
