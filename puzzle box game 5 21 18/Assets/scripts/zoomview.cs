﻿//nat's version, lerps back to original position for some reason, doesn't work
/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zoomview : MonoBehaviour{
	float rotSpeed = 80;

//  void OnMouseDown() {
//    GameObject.Find("Main Camera").GetComponent<CameraSwap>().boxMode = true;
//  }

	//if (GameObject.Find("puzzle box").GetComponent<CameraSwap>().boxMode == true) {
	private void OnMouseDrag() {
		this.transform.rotation = Quaternion.Euler(-Input.GetAxis ("Mouse X") * rotSpeed * Mathf.Deg2Rad, Input.GetAxis ("Mouse Y") * rotSpeed * Mathf.Deg2Rad, 0);
	}
	//}
}
*/

//my version, works but doesnt stop on z axis
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zoomview : MonoBehaviour
{
	float rotSpeed = 80;

    //void OnMouseDown()
    //{
    //    GameObject.Find("Main Camera").GetComponent<CameraSwap>().boxMode = true;
    //}

	//if (GameObject.Find("puzzle box").GetComponent<CameraSwap>().boxMode == true) {
	private void OnMouseDrag() {
		float rotX = Input.GetAxis ("Mouse X") * rotSpeed * Mathf.Deg2Rad;
		float rotY = Input.GetAxis ("Mouse Y") * rotSpeed * Mathf.Deg2Rad;
		transform.Rotate (Vector3.up, -rotX);
		transform.Rotate (Vector3.right, rotY);
		//this.transform.rotation = Quaternion.Euler(this.transform.rotation.x, this.transform.rotation.y, 0);
	}
	//}
}