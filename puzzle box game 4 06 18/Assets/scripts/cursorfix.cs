﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cursorfix : MonoBehaviour {

    public GameObject bigCamera;
    public GameObject pausemenu;
    
	// Update is called once per frame
	void Update () {
        if (bigCamera.GetComponent<CameraSwap>().boxMode == true || bigCamera.GetComponent<itemCameraSwap>().itemMode == true || pausemenu.activeInHierarchy == true) 
            {
            Cursor.lockState = CursorLockMode.None;
            } else
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
}
