﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class exitItemModeButtons : MonoBehaviour {

    public GameObject bigCamera;

    public void LeaveItemMode()
    {
        bigCamera.GetComponent<itemCameraSwap>().transitioningCameraFromItem = true;
        GetComponent<itemZoomView>().transitionFix = true;
    }
}
