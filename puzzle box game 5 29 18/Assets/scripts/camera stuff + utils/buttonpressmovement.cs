﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonpressmovement : MonoBehaviour {

    public GameObject buttonPressed;
    public float indentin;
    public float indentout;

    //public bool stayDown;
    //public bool buttonDown;



    
    //regular button press and release
    private void OnMouseDown()
    {
        transform.position += new Vector3(0, 0, indentin);
    }
    private void OnMouseUp()
    {
        transform.position += new Vector3(0, 0, indentout);
    }
    

    /*
    //failed 'stay down' code
    private void OnMouseDown()
    {
        Debug.Log("PRESS");
        if (stayDown)
        {           
            buttonDown = !buttonDown;
            Debug.Log(buttonDown);
            if (buttonDown)
            {
                transform.position += new Vector3(0, 0, indentin);
            }
            else
            {
                transform.position += new Vector3(0, 0, indentout);
            }
        } else
        {
            transform.position += new Vector3(0, 0, indentin);
        }
    }
    private void OnMouseUp()
    {
        if (!stayDown)
        {
            transform.position += new Vector3(0, 0, indentout);
        }
    }
    */
}
