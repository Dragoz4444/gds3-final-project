﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MurderPuzzleSolution : MonoBehaviour {

    public GameObject puzzlebox;

    private void OnMouseDown()
    {
        if (puzzlebox.GetComponent<PuzzleManager>().frontMurderPuzzle == false)
        {
            puzzlebox.GetComponent<PuzzleManager>().frontMurderPuzzle = true;
            Debug.Log("puzzle solved! murder mystery!");
        }
    }
}
