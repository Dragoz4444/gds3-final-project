﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemCameraSwap : MonoBehaviour {

    //[SerializeField] Transform one, two, cam;
    public bool itemMode;
    public bool transitioningCameraToItem;
    public bool transitioningCameraFromItem;
    public GameObject playerObject;
    public GameObject ItemMenuUI;
    public GameObject bigCamera;
    public GameObject itemClicked;

    // Update is called once per frame
    void Update()
    {
        if (bigCamera.GetComponent<CameraSwap>().boxMode == false)
        {
            if (transitioningCameraToItem == true)
            {
                transitioningCameraFromItem = false;
                itemMode = true;
                Cursor.lockState = CursorLockMode.None;
                ItemMenuUI.SetActive(true);
            }
        }
        if (bigCamera.GetComponent<CameraSwap>().boxMode == false)
        {
            if (transitioningCameraFromItem == true)
            {
                transitioningCameraToItem = false;
                itemMode = false;
                Cursor.lockState = CursorLockMode.Locked;
                ItemMenuUI.SetActive(false);
            }
        }

        if (itemMode == true && (transitioningCameraToItem == true || transitioningCameraFromItem == true))
        {
            if (bigCamera.GetComponent<CameraSwap>().boxMode == false)
            {
                transitioningCameraFromItem = false;
                transitioningCameraToItem = false;
            }
        }
    }

    public void LeaveItemMode()
    {
        transitioningCameraFromItem = true;
        itemClicked.GetComponent<itemZoomView>().transitionFix = true;
    }
}