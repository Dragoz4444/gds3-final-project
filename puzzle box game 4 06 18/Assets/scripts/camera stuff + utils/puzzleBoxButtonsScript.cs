﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puzzleBoxButtonsScript : MonoBehaviour {
    public GameObject bigCamera;

    public void LeaveBoxMode()
    {
        bigCamera.GetComponent<CameraSwap>().transitioningCameraFromBox = true;
        transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    public void LookRight()
    {
        transform.rotation = Quaternion.Euler(0, 90, 0);
    }

    public void LookLeft()
    {
        transform.rotation = Quaternion.Euler(0, -90, 0);
    }

    public void LookForward()
    {
        transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    public void LookBackward()
    {
        transform.rotation = Quaternion.Euler(0, 180, 0);
    }

    public void LookUp()
    {
        transform.rotation = Quaternion.Euler(90, 0, 0);
    }

    public void LookDown()
    {
        transform.rotation = Quaternion.Euler(-90, 0, 0);
    }
}
