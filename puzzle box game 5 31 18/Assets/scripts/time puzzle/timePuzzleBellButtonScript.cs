﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class timePuzzleBellButtonScript : MonoBehaviour {
    public bool triggeredTime;
    public GameObject puzzlebox;
    public GameObject lightObject;
    public Material[] material;
    Renderer rend;

    void Start()
    {
        rend = lightObject.GetComponent<Renderer>();
        rend.enabled = true;
        rend.sharedMaterial = material[0];
    }

    private void OnMouseDown()
    {
        //play bell sound
        if (triggeredTime == true)
        {
            if (puzzlebox.GetComponent<PuzzleManager>().backTimePuzzle == false)
            {
                puzzlebox.GetComponent<PuzzleManager>().backTimePuzzle = true;
                rend.sharedMaterial = material[1];
                //Debug.Log("puzzle solved! Clock/Time!");
                //play sound, change light colour to lit up etc
            }
        }
    }
}
