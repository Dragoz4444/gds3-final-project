﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwap : MonoBehaviour {
    [SerializeField] Transform one, two, cam;
    public bool boxMode;
	
    
    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(boxMode == true)
        {
            cam.transform.position = one.position;
            cam.transform.rotation = one.rotation;
        }
        else
        {
            cam.transform.position = two.position;
            cam.transform.rotation = two.rotation;
        }
	}
}
