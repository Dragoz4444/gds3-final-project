﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitPuzzleBoxModeButtonScript : MonoBehaviour {
    public GameObject bigCamera;
    
    void OnMouseDown()
    {
        bigCamera.GetComponent<CameraSwap>().transitioningCameraFromBox = true;
    }
}