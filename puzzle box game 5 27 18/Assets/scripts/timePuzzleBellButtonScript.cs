﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class timePuzzleBellButtonScript : MonoBehaviour {
    public bool triggeredTime;
    public GameObject puzzlebox;

    private void OnMouseDown()
    {
        //play bell sound
        if (triggeredTime == true)
        {
            if (puzzlebox.GetComponent<PuzzleManager>().backTimePuzzle == false)
            {
                puzzlebox.GetComponent<PuzzleManager>().backTimePuzzle = true;
                Debug.Log("puzzle solved! Clock/Time!");
                //play sound, change light colour to lit up etc
            }
        }
    }
}
