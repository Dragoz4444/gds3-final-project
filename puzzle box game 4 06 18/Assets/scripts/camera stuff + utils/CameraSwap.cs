﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwap : MonoBehaviour {
    [SerializeField] Transform one, two, cam;
    public bool boxMode;
    public bool transitioningCameraToBox;
    public bool transitioningCameraFromBox; 
    public GameObject playerObject;
    public GameObject BoxMenuUI;
    public GameObject puzzleBoxMum;
    public GameObject bigCamera;
    public GameObject puzzleboxparent;
    public GameObject boxmodepos;
    public GameObject walkmodepos;
    public GameObject audioObject;

    //puzzle objects
    public GameObject musicPuzzle;
    public GameObject numberPuzzle;
    public GameObject cypherPuzzle;


    // Update is called once per frame
    void Update()
    {
        if (bigCamera.GetComponent<itemCameraSwap>().itemMode == false)
        {
            if (transitioningCameraToBox == true)
            {
                transitioningCameraFromBox = false;
                cam.transform.position = one.position;
                cam.transform.rotation = one.rotation;
                boxMode = true;
                playerObject.SetActive(false);
                Cursor.lockState = CursorLockMode.None;
                BoxMenuUI.SetActive(true);
                puzzleBoxMum.SetActive(false);
                puzzleboxparent.transform.position = boxmodepos.transform.position;
            }
            if (transitioningCameraFromBox == true)
            {
                transitioningCameraToBox = false;
                playerObject.SetActive(true);
                cam.transform.position = two.position;
                cam.transform.rotation = two.rotation;
                boxMode = false;
                BoxMenuUI.SetActive(false);
                puzzleBoxMum.SetActive(true);
                puzzleboxparent.transform.position = walkmodepos.transform.position;

                //reset puzzles
                //music puzzle
                musicPuzzle.GetComponent<MusicPuzzle>().ResetPuzzle();

                //number puzzle
                numberPuzzle.GetComponent<numberPuzzleCode>().ResetPuzzle();

                //cypher puzzle
                cypherPuzzle.GetComponent<cypherPuzzleCode>().ResetPuzzle();
            }

            if (boxMode == true && (transitioningCameraToBox == true || transitioningCameraFromBox == true))
            {
                transitioningCameraFromBox = false;
                transitioningCameraToBox = false;
            }
        }
    }
}
