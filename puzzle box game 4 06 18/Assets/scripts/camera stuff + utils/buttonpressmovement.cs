﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonpressmovement : MonoBehaviour {

    public GameObject buttonPressed;
    public GameObject audioObject;
    public float indentin;
    public float indentout;

    public GameObject A_key, B_key, C_key, D_key, E_key, F_key, G_key;
    
    //regular button press and release
    private void OnMouseDown()
    {
        transform.position += new Vector3(0, 0, indentin);

        if (buttonPressed == A_key)
        {
            audioObject.GetComponent<audiomanager>().akeysource.Play();
        }
        else if (buttonPressed == B_key)
        {
            audioObject.GetComponent<audiomanager>().bkeysource.Play();
        }
        else if (buttonPressed == C_key)
        {
            audioObject.GetComponent<audiomanager>().ckeysource.Play();
        }
        else if (buttonPressed == D_key)
        {
            audioObject.GetComponent<audiomanager>().dkeysource.Play();
        }
        else if (buttonPressed == E_key)
        {
            audioObject.GetComponent<audiomanager>().ekeysource.Play();
        }
        else if (buttonPressed == F_key)
        {
            audioObject.GetComponent<audiomanager>().fkeysource.Play();
        }
        else if (buttonPressed == G_key)
        {
            audioObject.GetComponent<audiomanager>().gkeysource.Play();
        }
        else
        {
            audioObject.GetComponent<audiomanager>().buttonpressedsource.Play();
        }
    }
    private void OnMouseUp()
    {
        transform.position += new Vector3(0, 0, indentout);
        //audioObject.GetComponent<audiomanager>().buttonreleasedsource.Play();
    }
    
}
