﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwap : MonoBehaviour {
    [SerializeField] Transform one, two, cam;
    public bool boxMode;
    public bool transitioningCamera;
    public GameObject playerObject;


	
	// Update is called once per frame
	void Update () {
        if (transitioningCamera == true)
            {
                cam.transform.position = one.position;
                cam.transform.rotation = one.rotation;
                boxMode = true;
                playerObject.SetActive(false);
                Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            cam.transform.position = two.position;
            cam.transform.rotation = two.rotation;
            playerObject.SetActive(true);
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
}
