﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class numberPuzzleCode : MonoBehaviour {
    
    public static string correctCode = "seven_buttonfour_buttonthree_buttonnine_button";
    public static string playerCode = "";
    public static int totalDigits = 0;
    public GameObject puzzlebox;
    public GameObject lightObject;
    public GameObject audioObject;
    public Material[] material;
    Renderer rend;

    void Start()
    {
        rend = lightObject.GetComponent<Renderer>();
        rend.enabled = true;
        rend.sharedMaterial = material[0];
    }

    public void ButtonInfoEntered(string button)
    {
        if (totalDigits < 4)
        {
            totalDigits += 1;
            playerCode += button;
            Debug.Log(playerCode.ToString());
        }
        else
        {
            playerCode = "";
            totalDigits = 0;
            audioObject.GetComponent<audiomanager>().wronganswersource.Play();
        }
    }

    public void EnterButton()
    {
        if (totalDigits == 4)
        {
            if (playerCode == correctCode)
            {
                if (puzzlebox.GetComponent<PuzzleManager>().leftNumberPuzzle == false)
                {
                    puzzlebox.GetComponent<PuzzleManager>().leftNumberPuzzle = true;
                    rend.sharedMaterial = material[1];
                    audioObject.GetComponent<audiomanager>().bellsource.Play();
                }
            }
            else
            {
                playerCode = "";
                totalDigits = 0;
                audioObject.GetComponent<audiomanager>().wronganswersource.Play();
            }
        }
        else
        {
            audioObject.GetComponent<audiomanager>().wronganswersource.Play();
        }
    }

    public void ResetPuzzle()
    {
        playerCode = "";
        totalDigits = 0;
    }
}
