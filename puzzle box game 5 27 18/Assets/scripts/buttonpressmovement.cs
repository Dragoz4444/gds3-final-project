﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonpressmovement : MonoBehaviour {

    public GameObject buttonPressed;
    public float indentin;
    public float indentout;

    //public bool stayDown;
    //private bool stillDown;

    //regular button press and release
    private void OnMouseDown()
    {
        transform.position += new Vector3(0, 0, indentin);
    }
    private void OnMouseUp()
    {
        transform.position += new Vector3(0, 0, indentout);
    }

    /*failed 'stay down' code
    private void OnMouseDown()
    {
        if (stayDown == false)
        {
            transform.position += new Vector3(0, 0, indentin);
        }

        if (stayDown == true)
        {
            if (stillDown == false)
            {
                transform.position += new Vector3(0, 0, indentin);
                stillDown = true;
            }
            else
            {
                transform.position += new Vector3(0, 0, indentout);
                stillDown = false;
            }
        }
    }
    private void OnMouseUp()
    {
        if (stayDown == false)
        {
            transform.position += new Vector3(0, 0, indentout);
        }
    }
    */
}
