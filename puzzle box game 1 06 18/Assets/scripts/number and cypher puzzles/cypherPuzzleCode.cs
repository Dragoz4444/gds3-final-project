﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cypherPuzzleCode : MonoBehaviour {
    
    public static string correctcypherCode = "chinese_3_with_line_through_it_O_keyheart_W_keysideways_bird_foot_L_key";
    public static string playerinputCode = "";
    public static int totalnumberDigits = 0;
    public GameObject puzzlebox;
    public GameObject lightObject;
    public Material[] material;
    Renderer rend;

    void Start()
    {
        rend = lightObject.GetComponent<Renderer>();
        rend.enabled = true;
        rend.sharedMaterial = material[0];
    }

    public void ButtonInfoEntered(string button)
    {
            totalnumberDigits += 1;
            playerinputCode += button;
            Debug.Log(playerinputCode.ToString());
    }

    public void EnterButton()
    {
        if (playerinputCode == correctcypherCode)
        {
            if (puzzlebox.GetComponent<PuzzleManager>().bottomCypherPuzzle == false)
            {
                puzzlebox.GetComponent<PuzzleManager>().bottomCypherPuzzle = true;
                rend.sharedMaterial = material[1];
            }
        }
        else
        {
            playerinputCode = "";
            totalnumberDigits = 0;
            Debug.Log("ahaha fuck you you got it wrong");
        }
    }
    public void ResetPuzzle()
    {
        playerinputCode = "";
        totalnumberDigits = 0;
    }
}
