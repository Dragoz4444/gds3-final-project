﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MurderPuzzleSolution : MonoBehaviour {

    public GameObject puzzlebox;
    public GameObject lightObject;
    public Material[] material;
    Renderer rend;

    void Start()
    {
        rend = lightObject.GetComponent<Renderer>();
        rend.enabled = true;
        rend.sharedMaterial = material[0];
    }

    private void OnMouseDown()
    {
        if (puzzlebox.GetComponent<PuzzleManager>().frontMurderPuzzle == false)
        {
            puzzlebox.GetComponent<PuzzleManager>().frontMurderPuzzle = true;
            //Debug.Log("puzzle solved! murder mystery!");
            rend.sharedMaterial = material[1];
        }
    }
}