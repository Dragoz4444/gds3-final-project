﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MurderPuzzleSolution : MonoBehaviour {

    public GameObject puzzlebox;
    public GameObject lightObject;
    public GameObject audioObject;
    public Material[] material;
    Renderer rend;

    void Start()
    {
        rend = lightObject.GetComponent<Renderer>();
        rend.enabled = true;
        rend.sharedMaterial = material[0];
    }

    private void OnMouseDown()
    {
        if (puzzlebox.GetComponent<PuzzleManager>().frontMurderPuzzle == false)
        {
            puzzlebox.GetComponent<PuzzleManager>().frontMurderPuzzle = true;
            rend.sharedMaterial = material[1];
            audioObject.GetComponent<audiomanager>().bellsource.Play();
        }
    }
}