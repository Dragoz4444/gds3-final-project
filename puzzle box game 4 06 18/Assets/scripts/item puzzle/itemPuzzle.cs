﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemPuzzle : MonoBehaviour
{
    public float pressCount;
    public GameObject itemCode1, itemCode2, itemCode3, itemCode4;
    public GameObject puzzlebox;
    public GameObject lightObject;
    public GameObject audioObject;
    public GameObject applekey, dollarkey, spyglasskey, pawnkey, boxkey, featherkey, malachitekey, leafkey, mousekey, heartkey, tinderboxkey, 
        ringkey, daggerkey, bugkey, rabbitKey, spoonkey, toothkey, whistlekey, rosekey, necklacekey;
    public Material[] material;
    Renderer rend;

    void Start()
    {
        rend = lightObject.GetComponent<Renderer>();
        rend.enabled = true;
        rend.sharedMaterial = material[0];
    }

    private void Update()
    {
        if (pressCount > 4)
        {
            pressCount = 0;
            if (rabbitKey.GetComponent<itemPressButtonManager>().stillDown == true)
            {
                rabbitKey.GetComponent<itemPressButtonManager>().stillDown = false;
                rabbitKey.transform.position += new Vector3(0, 0, rabbitKey.GetComponent<itemPressButtonManager>().indentout);
            }
            if (necklacekey.GetComponent<itemPressButtonManager>().stillDown == true)
            {
                necklacekey.GetComponent<itemPressButtonManager>().stillDown = false;
                necklacekey.transform.position += new Vector3(0, 0, necklacekey.GetComponent<itemPressButtonManager>().indentout);
            }
            if (spoonkey.GetComponent<itemPressButtonManager>().stillDown == true)
            {
                spoonkey.GetComponent<itemPressButtonManager>().stillDown = false;
                spoonkey.transform.position += new Vector3(0, 0, spoonkey.GetComponent<itemPressButtonManager>().indentout);
            }
            if (applekey.GetComponent<itemPressButtonManager>().stillDown == true)
            {
                applekey.GetComponent<itemPressButtonManager>().stillDown = false;
                applekey.transform.position += new Vector3(0, 0, applekey.GetComponent<itemPressButtonManager>().indentout);
            }
            if (dollarkey.GetComponent<itemPressButtonManager>().stillDown == true)
            {
                dollarkey.GetComponent<itemPressButtonManager>().stillDown = false;
                dollarkey.transform.position += new Vector3(0, 0, dollarkey.GetComponent<itemPressButtonManager>().indentout);
            }
            if (spyglasskey.GetComponent<itemPressButtonManager>().stillDown == true)
            {
                spyglasskey.GetComponent<itemPressButtonManager>().stillDown = false;
                spyglasskey.transform.position += new Vector3(0, 0, spyglasskey.GetComponent<itemPressButtonManager>().indentout);
            }
            if (pawnkey.GetComponent<itemPressButtonManager>().stillDown == true)
            {
                pawnkey.GetComponent<itemPressButtonManager>().stillDown = false;
                pawnkey.transform.position += new Vector3(0, 0, pawnkey.GetComponent<itemPressButtonManager>().indentout);
            }
            if (boxkey.GetComponent<itemPressButtonManager>().stillDown == true)
            {
                boxkey.GetComponent<itemPressButtonManager>().stillDown = false;
                boxkey.transform.position += new Vector3(0, 0, boxkey.GetComponent<itemPressButtonManager>().indentout);
            }
            if (featherkey.GetComponent<itemPressButtonManager>().stillDown == true)
            {
                featherkey.GetComponent<itemPressButtonManager>().stillDown = false;
                featherkey.transform.position += new Vector3(0, 0, featherkey.GetComponent<itemPressButtonManager>().indentout);
            }
            if (malachitekey.GetComponent<itemPressButtonManager>().stillDown == true)
            {
                malachitekey.GetComponent<itemPressButtonManager>().stillDown = false;
                malachitekey.transform.position += new Vector3(0, 0, malachitekey.GetComponent<itemPressButtonManager>().indentout);
            }
            if (leafkey.GetComponent<itemPressButtonManager>().stillDown == true)
            {
                leafkey.GetComponent<itemPressButtonManager>().stillDown = false;
                leafkey.transform.position += new Vector3(0, 0, leafkey.GetComponent<itemPressButtonManager>().indentout);
            }
            if (mousekey.GetComponent<itemPressButtonManager>().stillDown == true)
            {
                mousekey.GetComponent<itemPressButtonManager>().stillDown = false;
                mousekey.transform.position += new Vector3(0, 0, mousekey.GetComponent<itemPressButtonManager>().indentout);
            }
            if (heartkey.GetComponent<itemPressButtonManager>().stillDown == true)
            {
                heartkey.GetComponent<itemPressButtonManager>().stillDown = false;
                heartkey.transform.position += new Vector3(0, 0, heartkey.GetComponent<itemPressButtonManager>().indentout);
            }
            if (tinderboxkey.GetComponent<itemPressButtonManager>().stillDown == true)
            {
                tinderboxkey.GetComponent<itemPressButtonManager>().stillDown = false;
                tinderboxkey.transform.position += new Vector3(0, 0, tinderboxkey.GetComponent<itemPressButtonManager>().indentout);
            }
            if (ringkey.GetComponent<itemPressButtonManager>().stillDown == true)
            {
                ringkey.GetComponent<itemPressButtonManager>().stillDown = false;
                ringkey.transform.position += new Vector3(0, 0, ringkey.GetComponent<itemPressButtonManager>().indentout);
            }
            if (daggerkey.GetComponent<itemPressButtonManager>().stillDown == true)
            {
                daggerkey.GetComponent<itemPressButtonManager>().stillDown = false;
                daggerkey.transform.position += new Vector3(0, 0, daggerkey.GetComponent<itemPressButtonManager>().indentout);
            }
            if (bugkey.GetComponent<itemPressButtonManager>().stillDown == true)
            {
                bugkey.GetComponent<itemPressButtonManager>().stillDown = false;
                bugkey.transform.position += new Vector3(0, 0, bugkey.GetComponent<itemPressButtonManager>().indentout);
            }
            if (toothkey.GetComponent<itemPressButtonManager>().stillDown == true)
            {
                toothkey.GetComponent<itemPressButtonManager>().stillDown = false;
                toothkey.transform.position += new Vector3(0, 0, toothkey.GetComponent<itemPressButtonManager>().indentout);
            }
            if (whistlekey.GetComponent<itemPressButtonManager>().stillDown == true)
            {
                whistlekey.GetComponent<itemPressButtonManager>().stillDown = false;
                whistlekey.transform.position += new Vector3(0, 0, whistlekey.GetComponent<itemPressButtonManager>().indentout);
            }
            if (rosekey.GetComponent<itemPressButtonManager>().stillDown == true)
            {
                rosekey.GetComponent<itemPressButtonManager>().stillDown = false;
                rosekey.transform.position += new Vector3(0, 0, rosekey.GetComponent<itemPressButtonManager>().indentout);
            }
        }

        if (itemCode1.GetComponent<itemPressButtonManager>().stillDown == true && itemCode2.GetComponent<itemPressButtonManager>().stillDown == true 
           && itemCode3.GetComponent<itemPressButtonManager>().stillDown == true && itemCode4.GetComponent<itemPressButtonManager>().stillDown == true)
        {
            if (puzzlebox.GetComponent<PuzzleManager>().rightItemPuzzle == false)
            {
                puzzlebox.GetComponent<PuzzleManager>().rightItemPuzzle = true;
                rend.sharedMaterial = material[1];
                audioObject.GetComponent<audiomanager>().bellsource.Play();
            }
        }
    }
}
