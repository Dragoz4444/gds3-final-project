﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemZoomView : MonoBehaviour {

    public float rotSpeed = 80;
    public GameObject bigCamera;
    public Vector3 itemorigin;
    public Quaternion itemoriginalRot;
    public GameObject ItemCamPos;
    public bool transitionFix;

    //on mouse down, move item to ItemCamPos's location and enable zoomed in /rotation view, 
    //then when clicking the UI exit button move it back.

    void Start()
    {
        //itemorigin = this.transform.position;
        //itemoriginalRot = this.transform.rotation;
    }

    void OnMouseDown()
    {
        if (bigCamera.GetComponent<CameraSwap>().boxMode == false)
        {
            if (bigCamera.GetComponent<itemCameraSwap>().itemMode == false)
            {
                itemorigin = this.transform.position;
                itemoriginalRot = this.transform.rotation;
                bigCamera.GetComponent<itemCameraSwap>().transitioningCameraToItem = true;
                transform.position = ItemCamPos.transform.position;
                transform.rotation = ItemCamPos.transform.rotation;
                Debug.Log("position moved");
            }
        }
    }

    private void OnMouseDrag()
    {
        if (bigCamera.GetComponent<CameraSwap>().boxMode == false)
        {
            if (bigCamera.GetComponent<itemCameraSwap>().itemMode == true)
            {
                float rotX = Input.GetAxis("Mouse X") * rotSpeed * Mathf.Deg2Rad;
                float rotY = Input.GetAxis("Mouse Y") * rotSpeed * Mathf.Deg2Rad;
                transform.Rotate(Vector3.up, rotX, Space.World);
                transform.Rotate(Vector3.right, -rotY, Space.World);
            }
        }
    }

    private void Update()
    {
        if (transitionFix == true) {
            if (bigCamera.GetComponent<itemCameraSwap>().transitioningCameraFromItem == true)
            {
                this.transform.position = itemorigin;
                this.transform.rotation = itemoriginalRot;
                Debug.Log("position moved back");
                transitionFix = false;
            }
        }
    }
}
