﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class clock : MonoBehaviour {

    public Transform hoursTransform, minutesTransform, secondsTransform;
    public bool hoursContinuous;
    public bool minutesContinuous;
    public bool secondsContinuous;

    const float degreesPerHour = 30f;
    const float degreesPerMinute = 6f;
    const float degreesPerSecond = 6f;

    void Update ()
    {
        if (hoursContinuous)
        {
            HoursContinuous();
        } 
        else
        {
            HoursDiscrete();
        }
        if (minutesContinuous)
        {
            MinutesContinuous();
        }
        else
        {
            MinutesDiscrete();
        }
        if (secondsContinuous)
        {
            SecondsContinuous();
        }
        else
        {
            SecondsDiscrete();
        }
    }

    void HoursContinuous () {
        TimeSpan time = DateTime.Now.TimeOfDay;
        hoursTransform.localRotation = 
        Quaternion.Euler(0f, (float)time.TotalHours * degreesPerHour, 0f);
    }

    void HoursDiscrete()
    {
        DateTime time = DateTime.Now;
        hoursTransform.localRotation =
        Quaternion.Euler(0f, time.Hour * degreesPerHour, 0f);
    }
    void MinutesContinuous()
    {
        TimeSpan time = DateTime.Now.TimeOfDay;
        minutesTransform.localRotation =
        Quaternion.Euler(0f, (float)time.TotalMinutes * degreesPerMinute, 0f);
    }

    void MinutesDiscrete()
    {
        DateTime time = DateTime.Now;
        minutesTransform.localRotation =
        Quaternion.Euler(0f, time.Minute * degreesPerMinute, 0f);
    }
    void SecondsContinuous()
    {
        TimeSpan time = DateTime.Now.TimeOfDay;
        secondsTransform.localRotation =
        Quaternion.Euler(0f, (float)time.TotalSeconds * degreesPerSecond, 0f);
    }

    void SecondsDiscrete()
    {
        DateTime time = DateTime.Now;
        secondsTransform.localRotation =
        Quaternion.Euler(0f, time.Second * degreesPerSecond, 0f);
    }
}
