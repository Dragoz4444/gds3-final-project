﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class handManager : MonoBehaviour {

    public GameObject bellButton;

    private void OnCollisionEnter(Collision collision)
    {
        bellButton.GetComponent<timePuzzleBellButtonScript>().triggeredTime = true;
    }
    private void OnCollisionExit(Collision collision)
    {
        bellButton.GetComponent<timePuzzleBellButtonScript>().triggeredTime = false;
    }
}
