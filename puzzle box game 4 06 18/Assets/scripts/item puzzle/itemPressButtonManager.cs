﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemPressButtonManager : MonoBehaviour {

    public GameObject buttonPressed;
    public GameObject buttonParent;
    public GameObject audioObject;
    public float indentin;
    public float indentout;
    public bool stillDown;

    private void OnMouseDown()
    {
        if (stillDown == false)
        {
            transform.position += new Vector3(0, 0, indentin);
            stillDown = true;
            buttonParent.GetComponent<itemPuzzle>().pressCount++;
            audioObject.GetComponent<audiomanager>().buttonpressedsource.Play();
        }
    }

    void Update()
    {
        if (buttonParent.GetComponent<itemPuzzle>().pressCount > 4)
        {
            if (stillDown == true)
            {
                transform.position += new Vector3(0, 0, indentout);
                stillDown = false;
            }
        }
    }
}
