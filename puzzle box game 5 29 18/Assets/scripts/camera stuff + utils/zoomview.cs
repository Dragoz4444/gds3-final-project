﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zoomview : MonoBehaviour
{
	public float rotSpeed = 80;
    public GameObject bigCamera;

        void OnMouseDown()
        {
            if (bigCamera.GetComponent<CameraSwap>().boxMode == false)
            {
                bigCamera.GetComponent<CameraSwap>().transitioningCameraToBox = true;
            }
        }


	    private void OnMouseDrag() {
            if (bigCamera.GetComponent<CameraSwap>().boxMode == true)
            {
                float rotX = Input.GetAxis("Mouse X") * rotSpeed * Mathf.Deg2Rad;
                float rotY = Input.GetAxis("Mouse Y") * rotSpeed * Mathf.Deg2Rad;
                transform.Rotate(Vector3.up, rotX, Space.World);
                transform.Rotate(Vector3.right, -rotY, Space.World);
                //transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, 0); :( removed z axis fix
            }  
        }
		
}

