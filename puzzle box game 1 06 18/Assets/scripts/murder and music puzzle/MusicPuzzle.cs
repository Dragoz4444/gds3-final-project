﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPuzzle : MonoBehaviour {

    public static string correctNotes = "E_keyD_keyE_keyD_keyE_keyB_keyD_keyC_keyA_keyC_keyE_keyA_keyB_key";
    public static string playerInputCode = "";
    public static int totalDigits = 13;
    public GameObject puzzlebox;
    public GameObject lightObject;
    public Material[] material;
    Renderer rend;

    void Start()
    {
        rend = lightObject.GetComponent<Renderer>();
        rend.enabled = true;
        rend.sharedMaterial = material[0];
    }

    private void OnMouseUp()
    {
        playerInputCode += gameObject.name;
        totalDigits += 1;
        Debug.Log(playerInputCode);

        if (totalDigits == 13)
        {
            if (playerInputCode == correctNotes)
            {
                if (puzzlebox.GetComponent<PuzzleManager>().topMusicPuzzle == false)
                {
                    puzzlebox.GetComponent<PuzzleManager>().topMusicPuzzle = true;
                    rend.sharedMaterial = material[1];
                }
            }
            else
            {
                playerInputCode = "";
                totalDigits = 0;
                Debug.Log("you didnt play the right song, dipshit");
            }
        }
        else if (totalDigits > 13) 
        {
            playerInputCode = "";
            totalDigits = 0;
            Debug.Log("you didnt play the right song, dipshit");
        }
    }
    

    public void ResetPuzzle()
    {
        playerInputCode = "";
        totalDigits = 0;
    }
}
