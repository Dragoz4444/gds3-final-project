﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleManager : MonoBehaviour {

    public bool frontMurderPuzzle, backTimePuzzle, leftNumberPuzzle, rightItemPuzzle, topMusicPuzzle, bottomCypherPuzzle;
    public GameObject drawer;
    public float speed = 5f;
    public bool moving = true;


    private void Update()
    {
        if (frontMurderPuzzle == true && backTimePuzzle == true && leftNumberPuzzle == true && rightItemPuzzle == true && topMusicPuzzle == true && bottomCypherPuzzle == true)
        {
            if (moving == true)
            {
                StartCoroutine(waitASec());
                drawer.transform.Translate(-speed, 0, 0);
            }
        }
    }

    IEnumerator waitASec()
    {
        yield return new WaitForSeconds(1);
        moving = false;
    }


}
