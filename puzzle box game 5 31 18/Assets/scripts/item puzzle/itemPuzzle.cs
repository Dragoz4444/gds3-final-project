﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemPuzzle : MonoBehaviour
{
    public float pressCount;
    public GameObject itemCode1, itemCode2, itemCode3, itemCode4;
    public GameObject puzzlebox;
    public GameObject lightObject;
    public Material[] material;
    Renderer rend;

    void Start()
    {
        rend = lightObject.GetComponent<Renderer>();
        rend.enabled = true;
        rend.sharedMaterial = material[0];
    }

    private void Update()
    {
        if (pressCount > 4)
        {
            pressCount = 0;
        }

        if (itemCode1.GetComponent<itemPressButtonManager>().stillDown == true && itemCode2.GetComponent<itemPressButtonManager>().stillDown == true 
           && itemCode3.GetComponent<itemPressButtonManager>().stillDown == true && itemCode4.GetComponent<itemPressButtonManager>().stillDown == true)
        {
            if (puzzlebox.GetComponent<PuzzleManager>().rightItemPuzzle == false)
            {
                puzzlebox.GetComponent<PuzzleManager>().rightItemPuzzle = true;
                rend.sharedMaterial = material[1];
            }
        }
    }
}
