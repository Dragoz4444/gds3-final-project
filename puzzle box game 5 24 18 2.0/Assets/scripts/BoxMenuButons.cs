﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxMenuButons : MonoBehaviour {
	public GameObject BoxMenuUI;
	public GameObject bigCamera;

	// Update is called once per frame
	void Update () {
		if (bigCamera.GetComponent<CameraSwap>().boxMode == true) {
			BoxMenuUI.SetActive (true);	
		} else {
            BoxMenuUI.SetActive(false);
		}
	}
}
